import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  equipments: [],
  error: null,
}

// getters
export const getters = {
  equipments: state => state.equipments,
}

// mutations
export const mutations = {

  [types.FETCH_EQUIPMENTS_SUCCESS] (state, equipments) {
    state.equipments = equipments
  },

  [types.FETCH_EQUIPMENTS_FAILURE] (state, error) {
    state.error = error
  },

}

// actions
export const actions = {

  async fetchEquipments ({ commit }) {
    try {
      const { data } = await axios.get('/api/equipments')

      commit(types.FETCH_EQUIPMENTS_SUCCESS, data)
    } catch (e) {
      commit(types.FETCH_EQUIPMENTS_FAILURE, e)
    }
  },

  updateUser ({ commit }, payload) {
    commit(types.UPDATE_USER, payload)
  },

  async logout ({ commit }) {
    try {
      await axios.post('/api/logout')
    } catch (e) { }

    commit(types.LOGOUT)
  },

  async fetchOauthUrl (ctx, { provider }) {
    const { data } = await axios.post(`/api/oauth/${provider}`)

    return data.url
  }
}
