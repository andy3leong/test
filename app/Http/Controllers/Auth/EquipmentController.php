<?php

namespace App\Http\Controllers\Auth;

use App\Equipment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EquipmentController extends Controller {

	public function __construct() {
	}

	public function index() {
		return Equipment::all();
	}

	public function store() {
		return Equipment::create(request()->only('name', 'company'));
	}

	public function csv() {
		$equipments = Equipment::all();
		$csvExporter = new \Laracsv\Export();
		$csvExporter->build($equipments, ['id', 'name', 'company'])->download();
	}
	// public function update(Equipment $equ) {
	// 	return $equ;
	// }

	// public function destroy(Equipment $equ) {
	// 	return $equ;
	// }
}
